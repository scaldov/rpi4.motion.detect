#!/usr/bin/python3

import os
import socket
import select
import time
import json
import argparse
import platform
import random

parser = argparse.ArgumentParser()
parser.add_argument('oldname');
parser.add_argument('newname');
args, unknown_args = parser.parse_known_args()

extensions = ['cflags', 'config', 'creator', 'creator.user', 'cxxflags', 'files', 'includes']
oldnames = [args.oldname + '.' + i for i in extensions]
newnames = [args.newname + '.' + i for i in extensions]

files = os.listdir()

for name in zip(oldnames, newnames):
    os.rename(name[0], name[1])
    print(name[0], ' --> ', name[1])
