#pragma once

#include <CLI/CLI.hpp>
#include <nlohmann/json.hpp>

#include "config.hh"

GConfig g_config;

void load_config(std::string path) {
    bool init = false;
    nlohmann::json json;
    try {
        auto f = std::ifstream(path);
        json << f;
        f.close();
    } catch(...) {
        init = true;
    }
    if(init) {
        json["regions"]["center"] = {10., 10., 90., 90.};
    }
    std::map<std::string, std::vector<float> > regions = json["regions"];
    for(auto& [key, value]: regions) {
        g_config.region_name.push_back(key);
        g_config.region.push_back({{value[0], value[1]}, {value[2], value[3]}});
    }
    try {
        g_config.dir = json["dir"];
    } catch (...) {
        g_config.dir = "/tmp/motion.detect/";
        json["dir"] = g_config.dir;
        init = true;
    }
    try {
        g_config.write_speed = json["write_speed"];
    } catch (...) {
        g_config.write_speed = 25.0;
        json["write_speed"] = g_config.write_speed;
        init = true;
    }
    try {
        g_config.frames = json["frames"];
    } catch (...) {
        g_config.frames = 30;
        json["frames"] = g_config.frames;
        init = true;
    }
    try {
        g_config.img_format = json["img_format"];
    } catch (...) {
        g_config.img_format = "tiff";
        json["img_format"] = g_config.img_format;
        init = true;
    }
    try {
        g_config.jpeg_quality = json["jpeg_quality"];
    } catch (...) {
        g_config.jpeg_quality = 90;
        json["jpeg_quality"] = g_config.jpeg_quality;
        init = true;
    }
    try {
        std::vector<int> res = json["resolution"];
        if(g_config.resolution.x == 0 || g_config.resolution.y ==0)
            g_config.resolution = {res[0], res[1]};
    } catch (...) {
        g_config.resolution = {1920,1080};
        json["resolution"] = {g_config.resolution.x, g_config.resolution.y};
        init = true;
    }
    if(init) {
        try {
            auto f = std::ofstream(path);
            f << json.dump(4);
            f.close();
        } catch(...) {
            std::cout << "could not create config " << path << std::endl;
            exit(-1);
        }
    }
}
