#pragma once

#include <glm/vec2.hpp>
#include <string>
#include <vector>
#include <map>
#include <chrono>

namespace glm {
template<typename T> struct rect{
    vec<2,T> lu, rd, wh;
    void UpdateRD(){ rd = lu + wh; }
    void UpdateLU(){ lu = rd - wh; }
    void UpdateWH(){ wh = rd - lu; }
};
}

template <typename T> std::ostream& operator<< (std::ostream& out, const std::vector<T>& v) {
    out << '[';
    for(auto x: v) {
        out << x << ",";
    }
    out << "\b]";
    return out;
}

template <typename T> std::ostream& operator<< (std::ostream& out, const glm::vec<2,T>& v) {
    out << '[' << v.x << "," << v.y << "]";
    return out;
}

template <typename T> std::ostream& operator<< (std::ostream& out, const glm::rect<T>& v) {
    out << '[' << v.lu.x << "," << v.lu.y << ","  << v.rd.x << "," << v.rd.y << "]";
    return out;
}

