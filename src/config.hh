#pragma once
#include "types.hh"

struct GConfig
{
    std::vector<glm::rect<float> > region;
    std::vector<std::string> region_name;
    std::string dir;
    glm::ivec2 resolution = {0, 0};
    std::string img_format;
    int jpeg_quality;
    int frames;
    float write_speed;
    int threshold;
};

extern GConfig g_config;

extern void load_config(std::string path);
