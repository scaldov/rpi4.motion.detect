#pragma once

#include <thread>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/videoio/videoio.hpp>
#include <filesystem>
#include <glm/vec2.hpp>
#include "types.hh"
#include "config.hh"
#include <queue>
#include <mutex>
#include <condition_variable>
#include <atomic>

class FrameSink
{
    struct Format {
        const static int JPEG = 1;
        const static int TIFF = 2;
    };
    struct Frame {
        cv::Mat frame;
        std::string filename;
        int format;
    };
public:
    std::queue<Frame> fifo;
    std::recursive_mutex lock;
    std::condition_variable sync_cv;
    std::atomic_flag sync_af;
    std::thread thread;
    FrameSink();
    void WriteFrame(const Frame &frame);
    static void ThreadFunction(FrameSink *self);
    void PushFrame(const cv::Mat &frame, const std::string &filename, const std::string &format);

};
