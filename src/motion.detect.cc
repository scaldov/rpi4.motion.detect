#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/videoio/videoio.hpp>
#include <filesystem>
#include <CLI/CLI.hpp>
#include <glm/vec2.hpp>
#include "types.hh"
#include "config.hh"
#include "framesink.hh"

/// For the Raspberry Pi 64-bit Bullseye OS

std::vector<glm::rect<int> > g_region;

FrameSink g_framesink;

std::string gstreamer_pipeline(int capture_width, int capture_height, int framerate) {
    return
            " libcamerasrc ! video/x-raw, "
            " width=(int)" + std::to_string(capture_width) + ","
            " height=(int)" + std::to_string(capture_height) + ","
            " framerate=(fraction)" + std::to_string(framerate) +"/1 !"
            " appsink";
}

std::string gstreamer_pipeline_v4l2(int capture_width, int capture_height, int framerate) {
    return
            " v4l2src ! image/jpeg, "
            " width=(int)" + std::to_string(capture_width) + ","
            " height=(int)" + std::to_string(capture_height) + ","
            " framerate=(fraction)" + std::to_string(framerate) +"/1 !"
            " jpegdec ! videoconvert ! appsink";
}

int frame_diff(const cv::Mat &frame, const cv::Mat &prev_frame, const glm::rect<int> &rect, bool f_disp) {
    auto range_col = cv::Range(rect.lu.x, rect.rd.x);
    auto range_row = cv::Range(rect.lu.y, rect.rd.y);
    cv::Mat frame_crop = frame(range_row, range_col);
    cv::Mat prev_frame_crop = prev_frame(range_row, range_col);
    cv::Mat eval1, eval2, diff, thresh;
    cv::cvtColor(frame_crop, eval1, cv::COLOR_BGR2GRAY);
    cv::cvtColor(prev_frame_crop, eval2, cv::COLOR_BGR2GRAY);
    cv::absdiff(eval1, eval2, diff);
    cv::threshold(diff, thresh, 32, 1, cv::THRESH_BINARY);
    // draw difference on image
    if(f_disp) {
        cv::threshold(diff, eval2, 32, 192, cv::THRESH_BINARY);
        cv::cvtColor(eval2, eval1, cv::COLOR_GRAY2BGR);
        eval1.copyTo(frame_crop);
    }
    //
    int n_values = cv::countNonZero(thresh);
    return n_values;
}

uint64_t get_timestamp() {
    auto t = std::chrono::high_resolution_clock::now();
    return std::chrono::duration_cast<std::chrono::nanoseconds>(t.time_since_epoch()).count();
}

struct Frame {
    Frame() {}
    Frame(const Frame &frame) {
        frame.frame.copyTo(this->frame);
        this->timestamp = frame.timestamp;
    }
    cv::Mat frame;
    uint64_t timestamp;
};

struct Event {
    std::string dir;
    int cnt;
    Event(uint64_t timestamp) {
        cnt = 0;
        dir = g_config.dir + '/' + std::to_string(timestamp);
        std::filesystem::create_directories(dir);
    }
    bool writeFrame(const Frame &frame, int delta = 1) {
        g_framesink.PushFrame(frame.frame, dir + "/" + std::to_string(frame.timestamp) + "." + g_config.img_format, g_config.img_format);
        cnt += delta;
        return cnt == g_config.frames;
    }
};

int main(int argc, char **argv) {
    //pipeline parameters
    int framerate = 30 ;
    std::string path_config = "motion.detect.conf";
    bool f_v4l2 = false;
    bool f_preview = false;
    bool f_display = false;
    g_config.threshold = 300;
    CLI::App app("motion.detect");
    app.add_option("-c,--config", path_config, "config file");
    app.add_option("-t,--threshold", g_config.threshold, "motion threshold");
    app.add_option("--width", g_config.resolution.x, "capture width");
    app.add_option("--height", g_config.resolution.y, "capture height");
    app.add_flag("--v4l2", f_v4l2, "use v4l2 instead of libcamera");
    app.add_flag("-p,--preview", f_preview, "show preview window");
    app.add_flag("-d,--display", f_display, "display values in regions");
    CLI11_PARSE(app, argc, argv);

    std::list<Frame> framelist;
    std::list<Event> eventlist;
    std::vector<int> max_diff;

    load_config(path_config);

    printf("%d regions\n", g_config.region.size());
    for(int i = 0; i < g_config.region.size(); i ++) {
        glm::vec2 s = {g_config.resolution.x, g_config.resolution.y};
        s *= 0.01;
        glm::vec2 lu = g_config.region[i].lu * s;
        glm::vec2 rd = g_config.region[i].rd * s;
        g_region.push_back({lu, rd});
        std::cout << g_config.region_name[i] << " = " << g_config.region[i] << std::endl;
        max_diff.push_back(0);
    }

    //reset frame average
    std::string pipeline = f_v4l2 ?
                gstreamer_pipeline_v4l2(g_config.resolution.x, g_config.resolution.y, framerate)
              :
                gstreamer_pipeline(g_config.resolution.x, g_config.resolution.y, framerate);
    std::cout << "pipeline: \n\t" << pipeline << "\n\n\n";

    cv::VideoCapture cap(pipeline, cv::CAP_GSTREAMER);
    if(!cap.isOpened()) {
        std::cout<<"Failed to open camera."<<std::endl;
        return (-1);
    }

    if(f_preview)cv::namedWindow("Camera", cv::WINDOW_AUTOSIZE);
    bool is_different;
    int n_frame=1;
    int d_frames = 0; // frames after motion
    int m_frames = 0; // frames after motion

    while(true)
    {
        Frame frame;
        if (!cap.read(frame.frame)) {
            std::cout<<"Capture read error"<<std::endl;
            break;
        }
        frame.timestamp = get_timestamp();

        auto &frame_prev = *std::prev(framelist.end());
        framelist.push_back(Frame(frame));
        if(framelist.size() > g_config.frames) framelist.pop_front();

        if(framelist.size() < 2) continue;
//        printf("p=%ld, c=%ld\n", frame_prev.timestamp, frame.timestamp);

        is_different = false;
        for(int i = 0; i < g_config.region.size(); i ++) {
            int diff = frame_diff(frame.frame, frame_prev.frame, g_region[i], f_display);
            is_different |= diff > g_config.threshold;
            if(max_diff[i] < diff){
                d_frames = 0;
                max_diff[i] = diff;
            }
        }
        // create event and write all previous frames
        if (is_different && (m_frames > g_config.frames / 2)){
            m_frames = 0;
            std::cout << "start" << std::endl;
            auto ev = eventlist.emplace_back(Event(frame.timestamp));
            for(const auto &f: framelist) {
                ev.writeFrame(f, 0);
            }
        } else {
            m_frames += 1;
        }
        printf("frames=%d\n", m_frames);
        // traverse events, write current frame and delete event if it lasted
        for(auto it = eventlist.begin(); it != eventlist.end(); ++it) {
            if(it->writeFrame(*std::prev(framelist.end()))) it = eventlist.erase(it);
        }

        n_frame += 1;
        d_frames += 1;
//        m_frames += 1;
        //show frame
        for(int i = 0; i < g_config.region.size(); i ++) {
            cv::rectangle(frame.frame, cv::Point(g_region[i].lu.x, g_region[i].lu.y), cv::Point(g_region[i].rd.x, g_region[i].rd.y), cv::Scalar(255, 255, 255));
            std::string val_str = std::to_string(max_diff[i]) + "   ";
            cv::putText(frame.frame, val_str, cv::Point(g_region[i].lu.x, g_region[i].lu.y), cv::FONT_HERSHEY_SIMPLEX, 1, {255, 0, 255}, 2, cv::LINE_AA, false);
            if(d_frames > 100) max_diff[i] = 0;
        }
        if(f_preview) cv::imshow("CSI cam",frame.frame);

        char esc = cv::waitKey(1);
        if(esc == 27) break;
    }

    cap.release();
    cv::destroyAllWindows() ;
    return 0;
}
