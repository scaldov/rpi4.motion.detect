#include <thread>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/videoio/videoio.hpp>
#include <filesystem>
#include <glm/vec2.hpp>
#include "types.hh"
#include "config.hh"
#include "framesink.hh"

FrameSink::FrameSink() {
    thread = std::thread(FrameSink::ThreadFunction, this);
}

void FrameSink::PushFrame(const cv::Mat &frame, const std::string &filename, const std::string &format){
    lock.lock();
    fifo.push({frame, filename,
               format == "jpeg" ? Format::JPEG : format == "tiff" ? Format::TIFF : 0
              });
    lock.unlock();
//    printf("---------------------- %p\n", this);
    std::cout << "added " << filename << std::endl;
//    std::cout << "size " << fifo.size() << std::endl;
    sync_af.test_and_set();
    sync_af.notify_all();
    return;
}

void FrameSink::WriteFrame(const Frame &frame) {
    static unsigned const int TIFFTAG_COMPRESSION = 259, COMPRESSION_NONE = 1;
    std::vector<int> compression_params;
    if(frame.format == Format::JPEG) {
        compression_params = {
            cv::IMWRITE_JPEG_QUALITY, g_config.jpeg_quality,
            cv::IMWRITE_JPEG_PROGRESSIVE, 1,
            cv::IMWRITE_JPEG_OPTIMIZE, 0,
            cv::IMWRITE_JPEG_LUMA_QUALITY, g_config.jpeg_quality
        };
    } else {
        compression_params = {
          TIFFTAG_COMPRESSION, COMPRESSION_NONE
        };
    }
    cv::imwrite(frame.filename, frame.frame, compression_params);
}

void FrameSink::ThreadFunction(FrameSink *self){
    while(true) {
        self->sync_af.wait(false);
        self->sync_af.clear();
        std::cout << "size " << self->fifo.size() << std::endl;
        if(!self->fifo.empty()) {
            auto frame = self->fifo.front();
            self->WriteFrame(frame);
            std::cout << frame.filename << std::endl;
            self->lock.lock();
            self->fifo.pop();
            self->lock.unlock();
        }
    }
}
